#include <stdio.h> 
#include <unistd.h>
#include <math.h>

//define at compile with -DN=n
//https://stackoverflow.com/questions/53288150/how-to-add-define-in-makefile
//#define N 500000
#define F 0.45

//cuda help from https://developer.nvidia.com/blog/even-easier-introduction-cuda/

__global__ 
void lorenz96_k1(double *xs, double ks[][N])
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
	double xn, xnm1, xnm2, xnp1;
	
	for (int n = index; n < N; n += stride)
	{
		xn = xs[n];
		xnm1 = n == 0 ? xs[N-1] : xs[n-1];
		xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
		xnp1 = n == N-1 ? xs[0] : xs[n+1];

		ks[0][n] = xnm1 * (xnp1 - xnm2) - xn + F;
	}
}

__global__
void lorenz96_k2(double *xs, double ks[][N])
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
	double xn, xnm1, xnm2, xnp1;
	double c1;
	for (int n = index; n < N; n += stride)
	{
		xn = xs[n];
		xnm1 = n == 0 ? xs[N-1] : xs[n-1];
		xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
		xnp1 = n == N-1 ? xs[0] : xs[n+1];

		c1 = 1.0/4.0;
		xn += c1 * ks[0][n];
		xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
		xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
		xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

		ks[1][n] = xnm1 * (xnp1 - xnm2) - xn + F;
	}
}

__global__
void lorenz96_k3(double *xs, double ks[][N])
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
	double xn, xnm1, xnm2, xnp1;
	double c1, c2;
	for (int n = index; n < N; n += stride)
	{
		xn = xs[n];
		xnm1 = n == 0 ? xs[N-1] : xs[n-1];
		xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
		xnp1 = n == N-1 ? xs[0] : xs[n+1];

		c1 = 3.0/32.0;
		xn += c1 * ks[0][n];
		xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
		xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
		xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

		c2 = 9.0/32.0;
		xn += c2 * ks[1][n];
		xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
		xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
		xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

		ks[2][n] = xnm1 * (xnp1 - xnm2) - xn + F;
	}
}

__global__
void lorenz96_k4(double *xs, double ks[][N])
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
	double xn, xnm1, xnm2, xnp1;
	double c1, c2, c3;
	for (int n = index; n < N; n += stride)
	{
		xn = xs[n];
		xnm1 = n == 0 ? xs[N-1] : xs[n-1];
		xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
		xnp1 = n == N-1 ? xs[0] : xs[n+1];

		c1 = 1932.0/2197.0;
		xn += c1 * ks[0][n];
		xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
		xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
		xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

		c2 = -7200.0/2197.0;
		xn += c2 * ks[1][n];
		xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
		xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
		xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

		c3 = 7296.0/2197.0;
		xn += c3 * ks[2][n];
		xnm1 += c3 * (n == 0 ? ks[2][N-1] : ks[2][n-1]);
		xnm2 += c3 * (n == 0 ? ks[2][N-2] : n == 1 ? ks[2][N-1] : ks[2][n-2]);
		xnp1 += c3 * (n == N-1 ? ks[2][0] : ks[2][n+1]);

		ks[3][n] = xnm1 * (xnp1 - xnm2) - xn + F;
	}
}

__global__
void lorenz96_k5(double *xs, double ks[][N])
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
	double xn, xnm1, xnm2, xnp1;
	double c1, c2, c3, c4;
	for (int n = index; n < N; n += stride)
	{
		xn = xs[n];
		xnm1 = n == 0 ? xs[N-1] : xs[n-1];
		xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
		xnp1 = n == N-1 ? xs[0] : xs[n+1];

		c1 = 439.0/216.0;
		xn += c1 * ks[0][n];
		xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
		xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
		xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

		c2 = -8.0;
		xn += c2 * ks[1][n];
		xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
		xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
		xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

		c3 = 3680.0/513.0;
		xn += c3 * ks[2][n];
		xnm1 += c3 * (n == 0 ? ks[2][N-1] : ks[2][n-1]);
		xnm2 += c3 * (n == 0 ? ks[2][N-2] : n == 1 ? ks[2][N-1] : ks[2][n-2]);
		xnp1 += c3 * (n == N-1 ? ks[2][0] : ks[2][n+1]);

		c4 = -845.0/4104.0;
		xn += c4 * ks[2][n];
		xnm1 += c4 * (n == 0 ? ks[3][N-1] : ks[3][n-1]);
		xnm2 += c4 * (n == 0 ? ks[3][N-2] : n == 1 ? ks[3][N-1] : ks[3][n-2]);
		xnp1 += c4 * (n == N-1 ? ks[3][0] : ks[3][n+1]);

		ks[4][n] = xnm1 * (xnp1 - xnm2) - xn + F;
	}
}

__global__
void lorenz96_k6(double *xs, double ks[][N])
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
	double xn, xnm1, xnm2, xnp1;
	double c1, c2, c3, c4, c5;
	for (int n = index; n < N; n += stride)
	{
		xn = xs[n];
		xnm1 = n == 0 ? xs[N-1] : xs[n-1];
		xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
		xnp1 = n == N-1 ? xs[0] : xs[n+1];

		c1 = -8.0/27.0;
		xn += c1 * ks[0][n];
		xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
		xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
		xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

		c2 = 2.0;
		xn += c2 * ks[1][n];
		xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
		xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
		xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

		c3 = 3544.0/2565.0;
		xn += c3 * ks[2][n];
		xnm1 += c3 * (n == 0 ? ks[2][N-1] : ks[2][n-1]);
		xnm2 += c3 * (n == 0 ? ks[2][N-2] : n == 1 ? ks[2][N-1] : ks[2][n-2]);
		xnp1 += c3 * (n == N-1 ? ks[2][0] : ks[2][n+1]);

		c4 = 1859.0/4104.0;
		xn += c4 * ks[2][n];
		xnm1 += c4 * (n == 0 ? ks[3][N-1] : ks[3][n-1]);
		xnm2 += c4 * (n == 0 ? ks[3][N-2] : n == 1 ? ks[3][N-1] : ks[3][n-2]);
		xnp1 += c4 * (n == N-1 ? ks[3][0] : ks[3][n+1]);

		c5 = 11.0/40.0;
		xn += c5 * ks[2][n];
		xnm1 += c5 * (n == 0 ? ks[4][N-1] : ks[4][n-1]);
		xnm2 += c5 * (n == 0 ? ks[4][N-2] : n == 1 ? ks[4][N-1] : ks[4][n-2]);
		xnp1 += c5 * (n == N-1 ? ks[4][0] : ks[4][n+1]);

		ks[5][n] = xnm1 * (xnp1 - xnm2) - xn + F;
	}
}

__global__
void prep_np1(double *xs, double *xsp1_a, double *xsp1_b, double ks[][N])
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
	
	for (int n = index; n < N; n += stride)
	{
		//rk4
		xsp1_a[n] = xs[n] + 25.0/216.0 * ks[0][n] + 1408.0/2565.0 * ks[2][n] + 2197.0/4101.0 * ks[3][n] - 1.0/5.0 * ks[4][n];
		//rk5
		xsp1_b[n] = xs[n] + 16.0/135.0 * ks[0][n] + 6656.0/12825.0 * ks[2][n] + 28561.0/56430.0 * ks[3][n] - 9.0/50.0 * ks[5][n] + 2.0/55.0 * ks[5][n];
	}
}

int main()
{
	double *xs, *xsp1_a, *xsp1_b;
	double *ks; //2d array dim: 6 * N

	cudaMallocManaged(&xs, N * sizeof(double));
	cudaMallocManaged(&xsp1_a, N * sizeof(double));
	cudaMallocManaged(&xsp1_b, N * sizeof(double));
	cudaMallocManaged(&ks, 6 * N * sizeof(double));

	double hi = 0.05, h = 0.05;
	bool tol_check = 0;
	int i = 0;
	double tol = 0.0001;
	int n_iter = 300;

	int blockSize = 256;
	int numBlocks = (N + blockSize -1) / blockSize;
	
	double err, s = 1.0, l2;

	for (int n = 0; n < N; n++)
		xs[n] = F * 1.0;
	xs[0] += 0.01;

	while (i < n_iter)
	{

		printf("\r%d: %lf, %lf, %lf", i, xs[0], xs[1], xs[2]);
		fflush(stdout);

		//Use reinterpret_cast to save effort on doing indexing
		//https://stackoverflow.com/questions/51296245/cudamallocmanaged-for-2d-and-3d-array
		lorenz96_k1<<<numBlocks, blockSize>>>(xs, reinterpret_cast<double (*)[N]>(ks));
		cudaDeviceSynchronize();
		//printf("%lf\n", reinterpret_cast<double (*)[N]>(ks)[0][1]);
		lorenz96_k2<<<numBlocks, blockSize>>>(xs, reinterpret_cast<double (*)[N]>(ks));
		cudaDeviceSynchronize();
		//printf("%lf\n", reinterpret_cast<double (*)[N]>(ks)[1][1]);
		lorenz96_k3<<<numBlocks, blockSize>>>(xs, reinterpret_cast<double (*)[N]>(ks));
		cudaDeviceSynchronize();
		//printf("%lf\n", reinterpret_cast<double (*)[N]>(ks)[2][1]);
		lorenz96_k4<<<numBlocks, blockSize>>>(xs, reinterpret_cast<double (*)[N]>(ks));
		cudaDeviceSynchronize();
		//printf("%lf\n", reinterpret_cast<double (*)[N]>(ks)[3][1]);
		lorenz96_k5<<<numBlocks, blockSize>>>(xs, reinterpret_cast<double (*)[N]>(ks));
		cudaDeviceSynchronize();
		//printf("%lf\n", reinterpret_cast<double (*)[N]>(ks)[4][1]);
		lorenz96_k6<<<numBlocks, blockSize>>>(xs, reinterpret_cast<double (*)[N]>(ks));
		cudaDeviceSynchronize();
		//printf("%lf\n", reinterpret_cast<double (*)[N]>(ks)[5][1]);

		//prepare n+1
		prep_np1<<<numBlocks, blockSize>>>(xs, xsp1_a, xsp1_b, reinterpret_cast<double (*)[N]>(ks));
		cudaDeviceSynchronize();
		//printf("%lf\n", xsp1_a[0]);

		//check tolerance for adaptive step size
		tol_check = 0;
		l2 = 0;
		for (int n = 0; n < N; n++)
		{
			err = xsp1_b[n] - xsp1_b[n];
			tol_check |= err >= tol; 
			l2 += err * err;
		}
		s = 0.84 * pow(tol / pow(l2, 0.5), 0.25);

		if (!tol_check)
		{
			//if tol_check == 0 then we update xs to xsp1_b
			for (int n = 0; n < N; n++)
				xs[n] = xsp1_b[n];
			i++; //the reason we are using a while. Only increment i if the tolerance is ok
			h = hi;
		}
		else
			h *= s; //if tol_check then decrease the mesh size

	}

	printf("\n");

	//clean up
	cudaFree(xs);
	cudaFree(xsp1_b);
	cudaFree(xsp1_a);
	cudaFree(ks);

	return 0;
}