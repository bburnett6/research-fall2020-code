#Make tutorial:
#https://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/

N=100000
CXX=g++
NVCC=nvcc

serial: lorenz96_rkf45.cpp 
	$(CXX) -Wall -o lorenz96.x lorenz96_rkf45.cpp -lm -DN=$(N)

omp: lorenz96_omprkf45.cpp 
	$(CXX) -Wall -o lorenz96_omp.x lorenz96_omprkf45.cpp -fopenmp -lm -DN=$(N)

cuda: lorenz96_cudarkf45.cu 
	$(NVCC) -o lorenz96_cuda.x lorenz96_cudarkf45.cu -lm -DN=$(N)

clean: 
	rm -f lorenz96.x lorenz96_omp.x lorenz96_cuda.x