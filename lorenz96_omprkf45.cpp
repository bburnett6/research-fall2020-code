#include <stdio.h> 
#include <unistd.h>
#include <math.h>
#include <omp.h>

//define at compile with -DN=n
//https://stackoverflow.com/questions/53288150/how-to-add-define-in-makefile
//#define N 500000

double lorenz96(double xn, double xnm1, double xnm2, double xnp1, double F)
{
	return xnm1 * (xnp1 - xnm2) - xn + F;
}

void lorenz96_rkf45(int n_iter, double tol)
{
	/*
	for N > 100000, the arrays used here cannot fit inside the stack.
	We use dynamic arrays to place them on the heap instead.

	https://stackoverflow.com/questions/1847789/segmentation-fault-on-large-array-sizes
	http://www.cplusplus.com/reference/new/operator%20delete%5B%5D/
	https://stackoverflow.com/questions/936687/how-do-i-declare-a-2d-array-in-c-using-new
	*/
	double *xs = new double[N];
	double F = 0.45;
	for (int n = 0; n < N; n++)
		xs[n] = F * 1.0;
	xs[0] += 0.01;
	double *xsp1_a = new double[N];
	double *xsp1_b = new double[N];
	double hi = 0.05, h = 0.05;
	double **ks = new double*[6];
	for (int k = 0; k < 6; k++)
		ks[k] = new double[N];
	bool tol_check = 0;
	int i = 0;
	double xn, xnm1, xnm2, xnp1;
	double c1, c2, c3, c4, c5;
	double err, s = 1.0, l2;

	omp_set_dynamic(0);
	omp_set_num_threads(omp_get_num_procs());
	#pragma omp parallel shared(xs, xsp1_a, xsp1_b, ks, i)
	{
		while (i < n_iter)
		{
			#pragma omp single
			{
				printf("\r%d: %lf, %lf, %lf", i, xs[0], xs[1], xs[2]);
				fflush(stdout);
				//fflush(stdout);
				if (tol_check)
					h *= s;
				else
				{
					i++; //the reason we are using a while. Only increment i if the tolerance is ok
					h = hi;
				}
			}
			//k1
			#pragma omp for private(xn, xnm1, xnm2, xnp1, c1, c2, c3, c4, c5)
			for (int n = 0; n < N; n++)
			{
				xn = xs[n];
				xnm1 = n == 0 ? xs[N-1] : xs[n-1];
				xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
				xnp1 = n == N-1 ? xs[0] : xs[n+1];

				ks[0][n] = lorenz96(xn, xnm1, xnm2, xnp1, F);
			}
			//k2
			#pragma omp for private(xn, xnm1, xnm2, xnp1, c1, c2, c3, c4, c5)
			for (int n = 0; n < N; n++)
			{
				xn = xs[n];
				xnm1 = n == 0 ? xs[N-1] : xs[n-1];
				xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
				xnp1 = n == N-1 ? xs[0] : xs[n+1];

				c1 = 1.0/4.0;
				xn += c1 * ks[0][n];
				xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
				xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
				xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

				ks[1][n] = lorenz96(xn, xnm1, xnm2, xnp1, F);
			}
			//k3
			#pragma omp for private(xn, xnm1, xnm2, xnp1, c1, c2, c3, c4, c5)
			for (int n = 0; n < N; n++)
			{
				xn = xs[n];
				xnm1 = n == 0 ? xs[N-1] : xs[n-1];
				xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
				xnp1 = n == N-1 ? xs[0] : xs[n+1];

				c1 = 3.0/32.0;
				xn += c1 * ks[0][n];
				xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
				xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
				xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

				c2 = 9.0/32.0;
				xn += c2 * ks[1][n];
				xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
				xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
				xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

				ks[2][n] = lorenz96(xn, xnm1, xnm2, xnp1, F);
			}
			//k4
			#pragma omp for private(xn, xnm1, xnm2, xnp1, c1, c2, c3, c4, c5)
			for (int n = 0; n < N; n++)
			{
				xn = xs[n];
				xnm1 = n == 0 ? xs[N-1] : xs[n-1];
				xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
				xnp1 = n == N-1 ? xs[0] : xs[n+1];

				c1 = 1932.0/2197.0;
				xn += c1 * ks[0][n];
				xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
				xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
				xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

				c2 = -7200.0/2197.0;
				xn += c2 * ks[1][n];
				xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
				xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
				xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

				c3 = 7296.0/2197.0;
				xn += c3 * ks[2][n];
				xnm1 += c3 * (n == 0 ? ks[2][N-1] : ks[2][n-1]);
				xnm2 += c3 * (n == 0 ? ks[2][N-2] : n == 1 ? ks[2][N-1] : ks[2][n-2]);
				xnp1 += c3 * (n == N-1 ? ks[2][0] : ks[2][n+1]);

				ks[3][n] = lorenz96(xn, xnm1, xnm2, xnp1, F);
			}
			//k5
			#pragma omp for private(xn, xnm1, xnm2, xnp1, c1, c2, c3, c4, c5)
			for (int n = 0; n < N; n++)
			{
				xn = xs[n];
				xnm1 = n == 0 ? xs[N-1] : xs[n-1];
				xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
				xnp1 = n == N-1 ? xs[0] : xs[n+1];

				c1 = 439.0/216.0;
				xn += c1 * ks[0][n];
				xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
				xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
				xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

				c2 = -8.0;
				xn += c2 * ks[1][n];
				xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
				xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
				xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

				c3 = 3680.0/513.0;
				xn += c3 * ks[2][n];
				xnm1 += c3 * (n == 0 ? ks[2][N-1] : ks[2][n-1]);
				xnm2 += c3 * (n == 0 ? ks[2][N-2] : n == 1 ? ks[2][N-1] : ks[2][n-2]);
				xnp1 += c3 * (n == N-1 ? ks[2][0] : ks[2][n+1]);

				c4 = -845.0/4104.0;
				xn += c4 * ks[2][n];
				xnm1 += c4 * (n == 0 ? ks[3][N-1] : ks[3][n-1]);
				xnm2 += c4 * (n == 0 ? ks[3][N-2] : n == 1 ? ks[3][N-1] : ks[3][n-2]);
				xnp1 += c4 * (n == N-1 ? ks[3][0] : ks[3][n+1]);

				ks[4][n] = lorenz96(xn, xnm1, xnm2, xnp1, F);
			}
			//k6
			#pragma omp for private(xn, xnm1, xnm2, xnp1, c1, c2, c3, c4, c5)
			for (int n = 0; n < N; n++)
			{
				xn = xs[n];
				xnm1 = n == 0 ? xs[N-1] : xs[n-1];
				xnm2 = n == 0 ? xs[N-2] : n == 1 ? xs[N-1] : xs[n-2];
				xnp1 = n == N-1 ? xs[0] : xs[n+1];

				c1 = -8.0/27.0;
				xn += c1 * ks[0][n];
				xnm1 += c1 * (n == 0 ? ks[0][N-1] : ks[0][n-1]);
				xnm2 += c1 * (n == 0 ? ks[0][N-2] : n == 1 ? ks[0][N-1] : ks[0][n-2]);
				xnp1 += c1 * (n == N-1 ? ks[0][0] : ks[0][n+1]);

				c2 = 2.0;
				xn += c2 * ks[1][n];
				xnm1 += c2 * (n == 0 ? ks[1][N-1] : ks[1][n-1]);
				xnm2 += c2 * (n == 0 ? ks[1][N-2] : n == 1 ? ks[1][N-1] : ks[1][n-2]);
				xnp1 += c2 * (n == N-1 ? ks[1][0] : ks[1][n+1]);

				c3 = 3544.0/2565.0;
				xn += c3 * ks[2][n];
				xnm1 += c3 * (n == 0 ? ks[2][N-1] : ks[2][n-1]);
				xnm2 += c3 * (n == 0 ? ks[2][N-2] : n == 1 ? ks[2][N-1] : ks[2][n-2]);
				xnp1 += c3 * (n == N-1 ? ks[2][0] : ks[2][n+1]);

				c4 = 1859.0/4104.0;
				xn += c4 * ks[2][n];
				xnm1 += c4 * (n == 0 ? ks[3][N-1] : ks[3][n-1]);
				xnm2 += c4 * (n == 0 ? ks[3][N-2] : n == 1 ? ks[3][N-1] : ks[3][n-2]);
				xnp1 += c4 * (n == N-1 ? ks[3][0] : ks[3][n+1]);

				c5 = 11.0/40.0;
				xn += c5 * ks[2][n];
				xnm1 += c5 * (n == 0 ? ks[4][N-1] : ks[4][n-1]);
				xnm2 += c5 * (n == 0 ? ks[4][N-2] : n == 1 ? ks[4][N-1] : ks[4][n-2]);
				xnp1 += c5 * (n == N-1 ? ks[4][0] : ks[4][n+1]);

				ks[5][n] = lorenz96(xn, xnm1, xnm2, xnp1, F);
			}

			//prepare n+1
			#pragma omp for
			for (int n = 0; n < N; n++)
			{
				//rk4
				xsp1_a[n] = xs[n] + 25.0/216.0 * ks[0][n] + 1408.0/2565.0 * ks[2][n] + 2197.0/4101.0 * ks[3][n] - 1.0/5.0 * ks[4][n];
				//rk5
				xsp1_b[n] = xs[n] + 16.0/135.0 * ks[0][n] + 6656.0/12825.0 * ks[2][n] + 28561.0/56430.0 * ks[3][n] - 9.0/50.0 * ks[5][n] + 2.0/55.0 * ks[5][n];
			}

			#pragma omp single
			{
				//check tolerance for adaptive step size
				tol_check = 0;
				l2 = 0;
				for (int n = 0; n < N; n++)
				{
					err = xsp1_b[n] - xsp1_b[n];
					tol_check |= err >= tol; 
					l2 += err * err;
				}
				s = 0.84 * pow(tol / pow(l2, 0.5), 0.25);

				if (!tol_check)
				{
					//if tol_check == 0 then we update xs to xsp1_b
					for (int n = 0; n < N; n++)
						xs[n] = xsp1_b[n];
				}
			}
		}
	}
	printf("\n");

	//clean up
	delete[] xs;
	delete[] xsp1_a;
	delete[] xsp1_b;
	for (int k = 0; k < 6; k++)
		delete[] ks[k];
	delete[] ks;
}

int main()
{

	lorenz96_rkf45(300, 0.0001);

	return 0;
}