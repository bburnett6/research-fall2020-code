import energyusage as eu 
import subprocess as sp 
import matplotlib.pyplot as plt 
import time

def run_cpu():
	cmd = f'./lorenz96.x'
	output = sp.run([cmd], stdout=sp.DEVNULL)

def run_omp():
	cmd = f'./lorenz96_omp.x'
	output = sp.run([cmd], stdout=sp.DEVNULL, timeout=30)

def run_gpu():
	cmd = f'./lorenz96_cuda.x'
	output = sp.run([cmd], stdout=sp.DEVNULL)

if __name__ == '__main__':
	ns = [100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000]
	en_cpu = []
	en_omp = []
	en_gpu = []

	for n in ns:
		comp_cmd = f'make serial N={n}'
		print(comp_cmd)
		output = sp.run(['bash', '-c', comp_cmd], stdout=sp.DEVNULL)
		time.sleep(0.5)
		c = eu.evaluate(run_cpu, energyOutput=True, printToScreen=False)
		en_cpu.append(c[1])

		comp_cmd = f'make omp N={n}'
		print(comp_cmd)
		output = sp.run(['bash', '-c', comp_cmd], stdout=sp.DEVNULL)
		time.sleep(0.5)
		o = eu.evaluate(run_omp, energyOutput=True, printToScreen=False)
		en_omp.append(o[1])

		comp_cmd = f'make cuda N={n}'
		print(comp_cmd)
		output = sp.run(['bash', '-c', comp_cmd], stdout=sp.DEVNULL)
		time.sleep(0.5)
		g = eu.evaluate(run_gpu, energyOutput=True, printToScreen=False)
		en_gpu.append(g[1])
		
	plt.loglog(ns, en_cpu, label='CPU')
	plt.loglog(ns, en_omp, label='OMP')
	plt.loglog(ns, en_gpu, label='GPU')
	plt.xlabel('Problem Size N')
	plt.ylabel('Energy Used (kWh)')
	plt.title('Lorenz 96 Using RKF45 Power Scaling')
	plt.legend()
	plt.show()
	#plt.savefig('energyscaling.png')