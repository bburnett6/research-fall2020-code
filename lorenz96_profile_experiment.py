import energyprofiler as ep  
import subprocess as sp 
import time 

def run_cpu():
	cmd = f'./lorenz96.x'
	output = sp.run([cmd], stdout=sp.DEVNULL)

def run_omp():
	cmd = f'./lorenz96_omp.x'
	output = sp.run([cmd], stdout=sp.DEVNULL, timeout=30)

def run_gpu():
	cmd = f'./lorenz96_cuda.x'
	output = sp.run([cmd], stdout=sp.DEVNULL)

if __name__ == '__main__':
	mon = ep.EnergyProfiler()
	mon.start()
	time.sleep(5)
	run_cpu()
	time.sleep(5)
	run_omp()
	time.sleep(5)
	run_gpu()
	time.sleep(5)
	mon.stop()
	mon.plot("Lorenz 96 Power Profile for N=900000")
