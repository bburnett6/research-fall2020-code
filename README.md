# Code for the Lorenz96 sample problem used to test the EnergyProfile package

This code has been tested on a system using GCC8.1.0 built with OpenMP support and NVIDIA Cuda version 10.2. To compile each test run (N is optional and has a default set):

```bash
#serial 
make serial N=100000
#OpenMP
make omp N=100000
#CUDA
make cuda N=100000
#Remove all
make clean 
```

The experiments included in this repository assume a python environment with the following installed:

```bash
#Matplotlib and EnergyUsage
pip install matplotlib energyusage
#EnergyProfiler (isn't on pypi so install directly from repository)
pip install git+https://gitlab.com/bburnett6/energyprofiler.git@78f636eb4196438123d7eb702a01503d5db94f46
```

The experiments can then be run with

```bash
python lorenz96_scaling_experiment.py 
python lorenz96_profile_experiment.py #note this assumes a compiled version of each test exists
```

Running these experiments should then display plots resembling the following:

![scaling](energyscaling.png)
![profile](energyprofile.png)